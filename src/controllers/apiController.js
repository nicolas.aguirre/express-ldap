const models = require('../database/models');
// const baseDir = 'https://service-portalnews.herokuapp.com/img/uploads/'
const baseDir = 'https://adminportal.utp.edu.co/img/uploads/'

// FORMAT DATE 
const formatDate = (fecha) => {
    const date = new Date(fecha);
    const year = date.getFullYear();
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    return `${year}-${month}-${day}`;
}

// GET ALL NEWS 
const getAllNews = async ( req,res ) => {
    try {

        let dataNews = await models.New.findAll({
            attributes: ['id','title','summary','image','link','createdAt', 'updatedAt'],
            raw:true
        })         
        
        dataNews.forEach(item => {                        
            item.image = `${baseDir}${item.image}`
            item.formatCreateAt = formatDate(item.createdAt)
            item.formatUpdatedAt = formatDate(item.updatedAt)
        });

        res.json(dataNews)

    }catch(error){
        console.log('Ha ocurrido un error: ' + error);
    }
}

// GET NEW BY ID
const getInfoNewById = async( req, res ) => {
    try {
        
        const idLocal = req.params.id;
        let dataInfoNew = await models.New.findOne({
            where: { id: idLocal },
            raw:true
        })

        dataInfoNew.image = `${baseDir}${dataInfoNew.image}`
        dataInfoNew.formatCreateAt = formatDate(dataInfoNew.createdAt)
        dataInfoNew.formatUpdatedAt = formatDate(dataInfoNew.updatedAt)
        
        res.json(dataInfoNew)

    }catch(error){
        console.log('Ha ocurrido un error: ' + error);
    }
}

// GET ALL NEWS 
const getAllCategories = async ( req,res ) => {
    try {

        let dataCategories = await models.Category.findAll({
            attributes: ['id','name','createdAt', 'updatedAt'],
            raw:true
        })

        dataCategories.forEach(item => {                        
            item.formatCreateAt = formatDate(item.createdAt)
            item.formatUpdatedAt = formatDate(item.updatedAt)
        });

        res.json(dataCategories)

    }catch(error){
        console.log('Ha ocurrido un error: ' + error);
    }
}

// GET ALL NEWS ACTIVES FOR PORTAL
const getNewsForPortal = async ( req,res ) => {
    try {
        const quantityNews = parseInt(req.query.c)
        const countDataNew = await models.New.count({ where:{ activeForPortal:1,active:1 } })

        if(req.query.c && (countDataNew > quantityNews)){
            let dataNews = await models.New.findAll({
                where:{ activeForPortal:1,active:1 },
                attributes: ['id', 'title','summary', 'image', 'link', 'position', 'createdAt', 'updatedAt'],
                limit:quantityNews,
                order: [['updatedAt', 'DESC']],
                raw:true
            })         
            
            dataNews.forEach(item => {                        
                item.image = `${baseDir}${item.image}`
                item.formatCreateAt = formatDate(item.createdAt)
                item.formatUpdatedAt = formatDate(item.updatedAt)
            });
            res.json(dataNews)
        }
        else{
            let dataNews = await models.New.findAll({
                where:{ activeForPortal:1,active:1 },
                attributes: ['id','title','summary','image','link','position','createdAt', 'updatedAt'],
                raw:true
            })         
            
            dataNews.forEach(item => {                        
                item.image = `${baseDir}${item.image}`
                item.formatCreateAt = formatDate(item.createdAt)
                item.formatUpdatedAt = formatDate(item.updatedAt)
            });
            res.json(dataNews)
        }
    }catch(error){
        console.log('Ha ocurrido un error: ' + error);
    }
}

// GET ALL NEWS ACTIVES FOR PORTAL ORDER BY POSITION
const getNewsForPortalOrderByPosition = async ( req,res ) => {

    let quantityNews = 5
    let arrayOfComparision = [-1,-1,-1]
    const countDataNew = await models.New.count({ where:{ activeForPortal:1 , active:1 } })

    while (arrayOfComparision.includes(-1) && quantityNews <= countDataNew) {

        let dataNews = await models.New.findAll({
            where:{ activeForPortal:1,active:1 },
            attributes: ['id', 'title','summary', 'image', 'link', 'position', 'createdAt', 'updatedAt'],
            limit:quantityNews,
            order: [['updatedAt', 'DESC']],
            raw:true
        })      
        
        const congruencePosicion1 = dataNews.find(data => data.position === 1);
        const congruencePosicion2 = dataNews.find(data => data.position === 2);
        const congruencePosicion3 = dataNews.find(data => data.position === 3);
        
        if(congruencePosicion1 !== undefined && arrayOfComparision[0] === -1 ) arrayOfComparision[0] = congruencePosicion1 
        if(congruencePosicion2 !== undefined && arrayOfComparision[1] === -1 ) arrayOfComparision[1] = congruencePosicion2
        if(congruencePosicion3 !== undefined && arrayOfComparision[2] === -1 ) arrayOfComparision[2] = congruencePosicion3

        quantityNews += 5
    }

    const arregloSinNegativos = arrayOfComparision.filter(elemento => elemento !== -1);

    arregloSinNegativos.forEach(item => {                        
        item.image = `${baseDir}${item.image}`
        item.formatCreateAt = formatDate(item.createdAt)
        item.formatUpdatedAt = formatDate(item.updatedAt)
    });

    res.json(arregloSinNegativos)

}

module.exports = {
    getAllNews,
    getInfoNewById,
    getAllCategories,
    getNewsForPortal,
    getNewsForPortalOrderByPosition
}