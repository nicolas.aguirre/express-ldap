document.addEventListener('DOMContentLoaded', (e) => {

    const currentUrl = window.location.href;
    const searchText = "/admin/noticias/nuevo";
    let enableBtn = [];

    if (currentUrl.includes(searchText)) {
        enableBtn = [ 0, 0, 0]
    } else {
        enableBtn = [ 1, 1, 1]
    }

    const submitButton = document.getElementById("btn-enviar");
    const form = document.getElementById("create-new-form")
    const titleInput = document.getElementById('title')
    const linkInput = document.getElementById('link')
    const linkImageInput = document.getElementById('linkImage')
    const imageUploadInput = document.getElementById("imagen")
    const summaryInput = document.querySelector(".ck-content")

    

    const checkEnableBtn = () => {
        console.log(enableBtn)
        if (enableBtn.includes(0)) {
            submitButton.disabled = true;
        } else {
            submitButton.disabled = false;
        }
    }
    
    //title's validator 
    const titleValidator = (e) => {
        const titleValue = titleInput.value.trim()

        if (titleValue.length > 3) {
            titleInput.classList.remove('is-invalid')
            titleInput.classList.add('is-valid')
            enableBtn[0] = 1
        } else {
            titleInput.classList.remove('is-valid')
            titleInput.classList.add('is-invalid')
            enableBtn[0] = 0
        }
        checkEnableBtn();
    }

    // Summary's validator 
    const summaryValidator = (e) => {
        if(summaryInput.innerText.trim() === ''){  
            summaryInput.classList.add("is-invalid")
            enableBtn[1] = 0                 
        }else {
            summaryInput.classList.remove("is-invalid")
            enableBtn[1] = 1
        }
        checkEnableBtn()
    }

    // link's validator
    const linkValidator = (e) => {
        const linkValue = linkInput.value.trim()
        const urlPattern = /^https:\/\/[a-zA-Z0-9]+([\-\.]{1}[a-zA-Z0-9]+)*\.[a-zA-Z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;

        if (urlPattern.test(linkValue)) {
            linkInput.classList.remove('is-invalid')
            linkInput.classList.add('is-valid')
            enableBtn[2] = 1
        } else {
            linkInput.classList.remove('is-valid')
            linkInput.classList.add('is-invalid')
            enableBtn[2] = 0
        }
        checkEnableBtn();
    }

    // link of image validator
    const linkImageValidator = (e) => {
        const linkImageValue = linkImageInput.value.trim()
            
        if (linkImageValue !== "") {
            const urlPattern = /^https:\/\/[a-zA-Z0-9]+([\-\.]{1}[a-zA-Z0-9]+)*\.[a-zA-Z]{2,5}(:[0-9]{1,5})?(\/.*)?$/
            
            if (urlPattern.test(linkImageValue)) {
                linkImageInput.classList.remove("is-invalid")
                linkImageInput.classList.add("is-valid")
            } else {
                linkImageInput.classList.remove("is-valid")
                linkImageInput.classList.add("is-invalid")
            }
        } else {
            linkImageInput.classList.remove("is-valid", "is-invalid")
        }
    }

    // Image upload field's validator
    const imageUploadValidator = (e) => {
        const file = imageUploadInput.files[0]
                
        if (file) {
            const maxSizeInBytes = 2 * 1024 * 1024 // 2MB
            
            if (file.size <= maxSizeInBytes && file.type.startsWith("image/")) {
                imageUploadInput.classList.remove("is-invalid")
                imageUploadInput.classList.add("is-valid")

            } else {
                imageUploadInput.classList.remove("is-valid")
                imageUploadInput.classList.add("is-invalid")

                imageUploadInput.value = "" // Limpiar el campo de carga de archivos
            }
        } else {
            imageUploadInput.classList.remove("is-valid", "is-invalid")
        }
    }

    
    titleInput.addEventListener('input', titleValidator)
    linkInput.addEventListener('input', linkValidator)
    linkImageInput.addEventListener('input', linkImageValidator)
    imageUploadInput.addEventListener('change', imageUploadValidator)
    summaryInput.addEventListener('keyup',summaryValidator)

    

})